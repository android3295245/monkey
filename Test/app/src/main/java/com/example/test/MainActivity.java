package com.example.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        TextView T_escrito;
        Button b_suma, b_resta, b_vacio, b_multi, b_div, b_color;

        T_escrito = (TextView) findViewById(R.id.textview1);
        b_suma = (Button) findViewById(R.id.button1);
        b_resta = (Button) findViewById(R.id.button2);
        b_vacio = (Button) findViewById(R.id.button3);
        b_color = (Button) findViewById(R.id.button_colour);
        b_multi = (Button) findViewById(R.id.button4);
        b_div = (Button) findViewById(R.id.button5);

        b_suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T_escrito.setText("Suma");
                Log.i("tag","tocaste suma");
                b_color.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.design_default_color_primary_dark));
            }
        });


        b_resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T_escrito.setText("Resta");
                Log.d("tag2","tocaste resta");
                b_color.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_green_light));
            }
        });

        b_vacio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T_escrito.setText("Vacío");
                System.out.println("tocaste vacio");
                b_color.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), android.R.color.holo_red_dark));
            }
        });

        b_multi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    T_escrito.setText(String.valueOf(10 / 0));
                }
                catch (Exception ea)
                {
                    Toast.makeText(MainActivity.this, "problemita" + ea, Toast.LENGTH_SHORT).show();
                }
                b_color.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
            }
        });

        b_div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                T_escrito.setText("Division");
                b_color.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.black));
            }
        });
    }
}